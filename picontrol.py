#!/usr/bin/env python3
import os, shlex, subprocess, sys
from flask import Flask, session, request, render_template, redirect, url_for
from werkzeug.utils import secure_filename
from pprint import pprint
from player import Player
from log import log

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

pwd = os.path.abspath('.')
app.config['UPLOAD_DIR'] = os.path.join(pwd, 'useruploads')
app.config['PLAYER_SCRIPT_DIR'] = os.path.join(pwd, 'playerscripts')
app.config['PLAYER_SCRIPT'] = os.path.join(
    app.config['PLAYER_SCRIPT_DIR'], 'playme.sh')
app.config['PLAYER'] = 'mpv'
app.config['APP_LOG'] = os.path.join(pwd, 'app.log')
app.config['PLAYER_LOG'] = os.path.join(pwd, 'player.log')


player = Player(player=app.config['PLAYER'])
logger = log(app.config['APP_LOG'])
logger.info(msg='Starting app')


# functions
def template_w_files(template: str, path: str, kargs: object={}) -> str:
    '''
    get current list of files
    and render the template using them
    '''
    pprint(kargs)
    files_in_dir = os.listdir(app.config['UPLOAD_DIR'])
    return render_template(template,
                           userdir=app.config['UPLOAD_DIR'],
                           files=files_in_dir,
                           path=path,
                           menulinks=menulinks,
                           kargs=kargs)


def handle_fileupload(rq: object, _file: object) -> str:
    if 'fileinput' not in rq.files.keys() or _file.filename == '':
        return 'No files selected for upload'

    file = rq.files['fileinput']
    if file:  # TODO: if file.filename.endswith('.mp4')
        try:
            # save to hidden and rename
            filename = os.path.join(app.config['UPLOAD_DIR'],
                                    secure_filename(file.filename))
            filename_tmp = os.path.join(app.config['UPLOAD_DIR'],
                                        '.' + secure_filename(file.filename))
            file.save(filename_tmp)
            os.rename(filename_tmp, filename)
            return 'File {} successfully uploaded'.format(filename)
        except Exception as e:
            return 'File {} failed to upload'.format(filename)


def handle_filedelete(rq: object, f2delete: str) -> str:
    try:
        for _f in f2delete:
            filename = os.path.join(app.config['UPLOAD_DIR'],
                                    secure_filename(_f))
            os.remove(filename)
        f2delete_str = (", ").join(f2delete)
        return 'Files: {} deleted successfully'.format(f2delete_str)
    except Exception as e:
        return 'Files failed to be deleted'


def storeNtest_player(action: str) -> str:
    if action == 'test':
        process = subprocess.Popen(player.cmd,
                                   shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        # stdout, stderr = process.communicate()
        # logger.info(stdout)
        # logger.error(stderr)

        msg = 'Playing'
    elif action == 'store':
        try:
            script = app.config['PLAYER_SCRIPT']
            with open(script, 'w') as PLAYER_SCRIPT:
                PLAYER_SCRIPT.write('#!/bin/sh\n{}'.format(player.cmd))
            os.chmod(script, 0o777)
            msg = '<p>Settings saved successfully to: <code>{}<code>\
            </p><p>Full command: \
            <code>{}</code></p>'.format(script, player.cmd)
        except Exception as e:
            msg = 'Failed to save settings'
        # TODO: add to app.config['PLAYER_SCRIPT'] .xinitrc
    else:
        msg = 'Missing msg'
    # print(msg, action)
    return msg


# routes
@app.route('/')
def main():
    return render_template('main.html', path=request.path, menulinks=menulinks)


@app.route('/upload', methods=['GET', 'POST'])  # upload form post handler
def upload():
    if request.method == 'GET':
        return template_w_files('upload.html',
                                request.path,
                                kargs=request.args)
    elif request.method == 'POST':
        form = request.args.get('form')
        if form == 'upload':
            file = request.files['fileinput']  # uploading files
            msg = handle_fileupload(request, file)
        elif form == 'delete':
            todelete = request.form.getlist('todelete')
            msg = handle_filedelete(request, todelete)
        # rediredt to request.url without paramaters
        return redirect(url_for(request.path[1:], msg=msg))


@app.route('/settings', methods=['GET', 'POST'])
def settings():
    if request.method == 'GET':
        return template_w_files('settings.html',
                                request.path,
                                kargs=request.args)

    elif request.method == 'POST':
        if request.form.get('debug'):
            logfile = app.config['PLAYER_LOG']
        else:
            logfile = None
        print('DEBUG VALUES', request.form.get('debug'), logfile)

        player.player_set(file=os.path.join(app.config['UPLOAD_DIR'],
                                            request.form.getlist('file')[0]),
                          loop=request.form.get('loop'),
                          volume=request.form.get('volume'),
                          logfile=logfile,
                          )

        msg = storeNtest_player(action=request.form.get('submit'))
        logger.info(msg=player.__dict__)
        return redirect(url_for('settings', msg=msg, **player.__dict__))


with app.test_request_context():
    menulinks = {
        'main page': url_for('main'),
        'upload/remove media': url_for('upload'),
        'player settings': url_for('settings')
    }


@app.route('/version')
def version():
    version = "Python version: {}".format(sys.version)
    return version

if __name__ == '__main__':
    app.run(debug=True)
