# Pi as video playback / slideshow device

## TODO 
* **playback settings menu**    
    * single file / playlist
    * slide show / video player
    *  add to app.config['playback_script'] to .xinitrc
* mpv '--no-audio-display' for audio

* player logs: rotate externally    
* no image in audio 

    try adding:   'mpv --msg-level=all=v'
    to get log msgs 


# LOGGING
* JSON formatted logging
    * `pip install python-json-logger`  
    * read: https://www.datadoghq.com/blog/python-logging-best-practices/


# WSGI server:
GUNICORN (ip:port) : 

gunicorn3 --workers 7 --bind 0.0.0.0:5000  wsgi:app

* Number of workers in gunicorn
the documentation suggestion "cpu_count() * 2 + 1" thing is a suggestion, you can add more to it... or less as you deem necessary

```bash 
cpus=`cat /proc/cpuinfo | grep 'core id' | wc -l`; echo `expr `expr 2 \* $cpus`
```
* systemd service `services/picontrol.service` 
    * is bound to: `--bind 0.0.0.0:5000`  
* host: player.local enabled in /etc/hosts/
```bash
127.0.0.1       localhost
127.0.0.1       player.local
```
* Apache:
    - enable proxy modules: `a2enmod proxy proxy_ajp proxy_http rewrite deflate headers proxy_balancer proxy_connect proxy_html xml2enc`
    - create site config
```
<VirtualHost *:80>
        # servername added in /etc/hosts
        ServerName player.local
        ServerAlias www.player.local

        DocumentRoot "/home/andre/Documents/Projects/prototypes/Picontrol"

        ProxyPass / http://127.0.0.1:5000/
        ProxyPassReverse / http://127.0.0.1:5000/

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

    <Directory "/home/andre/Documents/Projects/prototypes/Picontrol/static">
        Order allow,deny
        Allow from all
        Options Indexes FollowSymLinks MultiViews
        Satisfy Any
    </Directory>

</VirtualHost>
```